Instant Questions
-----------------

This module is a Questions Widget that improve the user experience for a common
Questions repository classified by tags. It createas a dynamic search with AJAX
and autocomplete that leverage in a powerful Apache Solr search to retrieve the
Questions that match with specific keywords.

How it Works
------------

Dependencies
------------

The AJAX autocomplete function is implemented under the jQuery Autocomplete
plugin developed by Jörn Zaefferer but currently maintained by others due the
deprecation of this module for the new version integrated in jQuery UI library.
We choice this plugin instead the jQuery UI because is more light and easy to
install. Probably in futures version we integrate adding as dependency the
module jQuery UI of Drupal that make ready to use all that functions inside
Drupal.

- Webpage of the unmantained version:
  http://bassistance.de/jquery-plugins/jquery-plugin-autocomplete/
- Webpage of the alternate version:
  https://github.com/agarzola/jQueryAutocompletePlugin

Installation
------------

bdynamicField name="sps_*"  type="textSpell" indexed="true"  stored="true" multiValued="false"/>
