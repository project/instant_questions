<?php

function instant_questions_admin_settings() {
  $form = array();

  try {
    $solr = apachesolr_get_solr();
    $ping = @$solr->ping(variable_get('apachesolr_ping_timeout', 4));
    if (!$ping) {
      throw new Exception(t('No Solr instance available.'));
    }
    $solr->clearCache();
    $data = $solr->getLuke();
  }
  catch (Exception $e) {
    watchdog('Apache Solr', nl2br(check_plain($e->getMessage())), NULL, WATCHDOG_ERROR);
    drupal_set_message(nl2br(check_plain($e->getMessage())), "warning");
    $data->fields = array();
    return;
  }

  $field_names_options = array();
  $fields = (array)$data->fields;
  $field_names = array_keys($fields);
  if (!empty($field_names)) {
    sort($field_names);
    $field_names_options = array_combine($field_names, $field_names);
  }

  $node_types = node_get_types('names');
  $form['instant_questions_content_type'] = array(
    '#title' => t('Questions Content Type'),
    '#type' => 'select',
    '#multiple' => false,
    '#required' => true,
    '#options' => $node_types,
    '#description' => t('Select the content type that will be used to search Questions.'),
    '#default_value' => variable_get('instant_questions_content_type', 'questions'),
  );

  $vocabularies_options = array();
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vocabulary) {
    $vocabularies_options[$vocabulary->vid] = $vocabulary->name;
  }

  $form['instant_questions_topic_vocabulary'] = array(
    '#title' => t('Select the topic vocabulary'),
    '#type' => 'select',
    '#multiple' => false,
    '#required' => false,
    '#options' => $vocabularies_options,
    '#description' => t('Select the vocabulary related to the Questions content
    type that will be used as Topic filter in Questions searching.'),
    '#default_value' => variable_get('instant_questions_topic_vocabulary', ''),
  );

  $form['instant_questions_search_field'] = array(
    '#title' => t('Select the field to use for the keywords search'),
    '#type' => 'select',
    '#multiple' => true,
    '#required' => false,
    '#options' => $field_names_options,
    '#description' => t('This field will be used as the main field for keyword search and display in the Questions Widget results.'),
    '#default_value' => variable_get('instant_questions_search_field', ''),
  );

  $form['instant_questions_ignore_empty_topics'] = array(
    '#type' => 'checkbox',
    '#title' => t("Don't show the terms without related nodes."),
    '#required' => false,
    '#default_value' => variable_get('instant_questions_ignore_empty_topics', 0),
  );

  return system_settings_form($form);
}

function instant_questions_admin_settings_validate($form, &$form_state) {
  $vid = $form_state['values']['instant_questions_topic_vocabulary'];
  $questions_ct = $form_state['values']['instant_questions_content_type'];

  if (!$vid) {
    form_set_error('instant_questions_topic_vocabulary', t('The topic vocabulary is required.'));
  }

  $taxonomynode_settings = variable_get("taxonomynode_{$vid}", NULL);
  if (empty($taxonomynode_settings['content_type'])) {
    form_set_error('instant_questions_topic_vocabulary', t('The topic vocabulary must allow taxonomynode creation to make possible the terms indexation. Configure your vocabulary as taxonomynode.'));
  }

  $topic_vocabulary = taxonomy_vocabulary_load($vid);
  if (!isset($topic_vocabulary->nodes) || !isset($topic_vocabulary->nodes[$questions_ct])) {
    form_set_error('instant_questions_topic_vocabulary', t('The topic vocabulary must be referenceable by the questions content type. Allow this vocabulary to be referenceable for questions content type.'));
  }
}
