<?php

/**
 * Implementation of hook_drush_help
 */
function instant_questions_drush_help($section) {
  switch ($section) {
   case 'drush:gen-term-nodes':
   return dt("Generate the term nodes for the taxonomy_node module.");
      break;
    default:
      break;
  }
}

/**
 * Implementation of hook_drush_command
 */
function instant_questions_drush_command() {
  $items = array();
  $items['gen-term-nodes'] = array(
    'description' => "Generate the term nodes for the taxonomy_node module.",
    'options' => array('--vid' => 'Vocabulary ID to process.'),
    'aliases' => array('gen-tn'),
    'callback'    => 'instant_questions_gen_term_nodes',
  );
  $items['merge-duplicated-terms'] = array(
    'description' => "Merge a duplicated terms in the same vocabulary.",
    'options' => array('--vid' => 'Vocabulary ID to process.'),
    'aliases' => array('merg-dt'),
    'callback'    => 'instant_questions_merge_duplicated_terms',
  );

  return $items;
}

/**  
 * Generate the taxonomy term nodes for specific vocabulary 
 */
function instant_questions_gen_term_nodes() {
  $vid = drush_get_option('vid');
  if (empty($vid)) {
    die("A valid vid is mandatory to generate the taxonomy nodes.");
  }

  echo "Generating the taxonomy nodes for vocabulary {$vid}\n";
  $result = db_query("SELECT t.* FROM {term_data} t LEFT JOIN {taxonomynode} tn ON t.tid = tn.tid WHERE t.vid = %d AND isnull(tn.tid)", $vid);

  $ct = 0;
  while ($row = db_fetch_object($result)) {
    $term = taxonomy_get_term($row->tid);
    $parents = taxonomy_get_parents($row->tid);
    _taxonomynode_toggle();
    _taxonomynode_create_node($row->tid, $vid, $term->name, array_keys($parents));
    _taxonomynode_toggle();
    $ct++;
  }

  echo "Created {$ct} taxonomy terms nodes.\n";
}


/**  
 * Identify taxonomy terms that are duplicated and merge them
 */
function instant_questions_merge_duplicated_terms() {
  module_load_include('inc', 'taxonomy_manager', 'taxonomy_manager.admin');

  $vid = drush_get_option('vid');
  if (empty($vid)) {
    die("A valid vid is mandatory to generate the taxonomy nodes.");
  }

  $result = db_query("SELECT * FROM {term_data} WHERE vid = %d", $vid);
  $lower_terms = array();

  while ($row = db_fetch_object($result)) {
    $term = strtolower($row->name);
    if (!isset($lower_terms[$term])) {
      $lower_terms[$term] = array();
    } 
    $lower_terms[$term][] = $row->tid;
  }

  foreach ($lower_terms as $name => $tids) {
    if (count($tids) > 1) {
      if (function_exists('taxonomy_manager_merge')) {
        echo "Merging duplicated term - {$name} - " . implode(',', $tids) . "\n";
        taxonomy_manager_merge($tids[0], $tids, array('collect_children' => 1, 'collect_relations' => 1));
      }
      else {
        die("Taxonomy merge module must be installed to use taxonomy_manager_merge helper function.");
      }
    }
  }
}
