/**
 * Adds the custom autocomplete widget behavior.
 */
Drupal.behaviors.instant_questions = function(context) {
   
  $(".instant-questions:not(.iq-processed)", context).addClass('iq-processed').autocomplete(Drupal.settings.instant_questions.path,
  {
    inputClass: "",
    loadingClass: "throbbing",
    // Do not select first suggestion by default.
    selectFirst: false,
    // Specify no matching as it wil be done on server-side.
    matchContains: false,
    matchSubset: false,
    // Maximum number of items to show in widget.
    max: 50,
    width: 652,
    scroll: true,
    scrollHeight: 460,
    // Data returned from server is JSON-encoded.
    dataType: "json",
    // Function to parse returned json into elements.
    parse: function(data) {
      if (data.length == 0) {
        $('.asklink').show();
        $('#instantquestions-content p').hide();
      }
      else {
        $('.asklink').hide();
      }
      return $.map(data, function(item) {
        return {
          data: item,
          value: item.title, // This will be shown in the options widget.
          //result: item.title // The actual value to put into the form element.
        }
      });
    },
    // Return the HTML to display in the options widget.
    formatItem: function(item) {
      var label = '';
      if (item.type == 'topic') {
        var label = 'Keyword';
      }
      else {
        var label = 'Question';
      }
      return item.title + ' ' + '<span class="topic">' + label + '</span>';
    },
    highlight: function(match, keywords) {
      // Escape any regexy type characters so they don't bugger up the other reg ex
      keywords = keywords.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1");
      // Join the terms with a pipe as an 'OR'
      keywords = keywords.split(' ').join('|');
      return match.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + keywords + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
    },
    extraParams: { topic: function(autocomplete) { 
      var tid = $('#iqtopicfilter').val();
      return(tid);
    }}, 
  }).result(function(item, element) {
    // Handle selection of an element in the autocomplete widget.
    // We should submit the widget's parent form.
    $('#edit-keywords').blur();
    //$.blockUI({ message: '<h3>Redirecting...</h3>' });
    
    if (element.type == 'topic') {
      $(this).val(element.title + ': ');
      $('#iqtopicfilter').val(element.key);
    }
    else {
      location.href = '/node/' + element.key;
    }
    
  }).addClass('form-autocomplete'); // Add Drupal autocomplete widget's style.  
  
  // Hide the instant questions form to begin with
  
  $("#askquestion-button-open:not(.iq-processed)").addClass('iq-processed').click(function(event) {
    event.preventDefault();
    if ( $("#instantquestions-content").css("display") == "block" ){
      $('#edit-keywords').focus();
    }
  });
};

$(document).ready(function() {
  // Put focus on the ask question button
  // when no results are found and down arrow is pressed
  $("#edit-keywords").keydown(function(e){
    if (e.keyCode == 40 && ($(".ac-results ul").size() == 0)) {
      $('#instantquestions-content a.asklink').focus();
      return false;
    }
  });
  
  $("#instantquestions-content a.asklink").keydown(function(e){
    // Put focus on the ask question form
    // when no results are found and up arrow is pressed
    if (e.keyCode == 38 && ($(".ac-results ul").size() == 0)) {
      $('#edit-keywords').focus();
      return false;
    }
  });
  
  $(".ac_results li").keydown(function(event) {
      alert(event.keyCode);
  });
});
