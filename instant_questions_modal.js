Drupal.behaviors.instantQuestionsModal = function() {
  var question = $("input.instant-questions").val();
  if ( question ) {
    question = question.replace(/.*?:/g, ""); 
    $("input.title").val(question);
  }

  // Clear email label on Ask Question form. 
  $('#instant-questions-add-question-form #edit-email').clearingInput({text: 'Your email address'});

};

